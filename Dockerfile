FROM ruby:slim-stretch
MAINTAINER info@coderat.cc

RUN apt-get update -qq && apt-get install -y \
    gnupg2 \
    curl
ADD https://dl.yarnpkg.com/debian/pubkey.gpg /tmp/yarn-pubkey.gpg
RUN apt-key add /tmp/yarn-pubkey.gpg && rm /tmp/yarn-pubkey.gpg
RUN echo "deb http://dl.yarnpkg.com/debian/ stable main" > /etc/apt/sources.list.d/yarn.list
RUN curl -sL https://deb.nodesource.com/setup_8.x |  bash -

RUN apt-get update -qq && apt upgrade -y && apt-get install -y \
	build-essential \
	libsqlite3-dev \
	yarn \
	nodejs 
RUN gem install bundler rails
RUN mkdir /cryptorat
WORKDIR /cryptorat
ADD Gemfile /cryptorat/Gemfile
ADD Gemfile.lock /cryptorat/Gemfile.lock
RUN bundle install --jobs 5 --retry 5 
#ENTRYPOINT ["bundle", "exec"]

