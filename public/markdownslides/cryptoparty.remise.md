name: inverse
layout: true
class: center, middle, inverse
---
# Cryptoparty Teil 1: 
# Internet und Browsing

.footnote3[ [cryptoparty.coderat.cc](https://cryptoparty.coderat.cc/)]
---
layout: false
class: normslide
.left-column[
  ## Themen
]
.right-column[
-----


 - Threatmodelling / Bedrohungsszenarien 

 - Technische Antworten

 - Was Metadaten über uns verraten

 - Fragen und Themen sammeln


]
.footnote[Wlan / remise-interrim]
.footnote2[Passwort / steht über der Bar]
.footnote3[ [cryptoparty.coderat.cc](https://cryptoparty.coderat.cc/)]
???
Notes for Presentation mode

---
layout: false
class: normslide
.left-column[
  ## Threatmodelling?
]
.right-column[
Um wen geht es?

 - Firmen, Plattformen

 - staatliche Überwachung

 - Cybercrime

]
.footnote[Wlan / remise-interrim]
.footnote2[Passwort / steht über der Bar]
.footnote3[ [cryptoparty.coderat.cc](https://cryptoparty.coderat.cc/)]
???
Notes for Presentation mode

---
layout: false
class: normslide
.left-column[
  ## Techn. Antworten
]
.right-column[
Technologien:

 - Anonymisierungsdienste

 - Browser-Addons

 - Transportverschlüsselung (SSL/TLS)

]
.footnote[Wlan / remise-interrim]
.footnote2[Passwort / steht über der Bar]
.footnote3[ [cryptoparty.coderat.cc](https://cryptoparty.coderat.cc/)]
???
Notes for Presentation mode

---
layout: false
class: normslide
.left-column[
 ## Techn. Antworten
 &nbsp;
 ## Anonymisierung
]
.right-column[
-------
* VPN: [thatoneprivacysite.net/vpn-section/](https://thatoneprivacysite.net/vpn-section/)

* Tor: 
<img style="width:115%"  src="/assets/tor-11db495d01a691f16470b84cfabcaa8f47433f50bf5bffcba8070b38ca1fcc27.jpg">

* Tails: 
 &nbsp;
<img style="width:50%"src="/assets/tails-d2c37709dd73458db081fae4be9fbcc13750419af87342ecd524a87be3c9e799.jpg">
]
.footnote[Wlan / remise-interrim]
.footnote2[Passwort / steht über der Bar]
.footnote3[ [cryptoparty.coderat.cc](https://cryptoparty.coderat.cc/)]
???
Notes for Presentation mode
---
layout: false
class: normslide
.left-column[
 ## Techn. Antworten
 &nbsp;
 ## Anonymisierung
 &nbsp;
 ## BrowserAddons
]
.right-column[
-------
Essential Addons:
* [Privacy Badger](https://www.eff.org/privacybadger)
* [HTTPS Everywhere](https://www.eff.org/https-everywhere)
* [uBlock Origin](https://addons.mozilla.org/de/firefox/addon/ublock-origin/)

Advanced:
* [uMatrix](https://addons.mozilla.org/de/firefox/addon/umatrix/)
* [Canvasblocker](https://addons.mozilla.org/de/firefox/addon/umatrix/)
]
.footnote[Wlan / remise-interrim]
.footnote2[Passwort / steht über der Bar]
.footnote3[ [cryptoparty.coderat.cc](https://cryptoparty.coderat.cc/)]
???
Notes for Presentation mode
---
layout: false
class: normslide
.left-column[
 ## Techn. Antworten
 &nbsp;
 ## Anonymisierung
 &nbsp;
 ## BrowserAddons
 &nbsp;
 ## HTTPS/TLS
]
.right-column[
-------
* Transportverschlüsselung im Browser
<img style="width:110%"  src="/assets/ssl-3f6e04faf6e1420a3f7489b17c5cc9fc8355f8d5bdbe7c1779c92ade74cf4548.png">
]
.footnote[Wlan / remise-interrim]
.footnote2[Passwort / steht über der Bar]
.footnote3[ [cryptoparty.coderat.cc](https://cryptoparty.coderat.cc/)]
???
Notes for Presentation mode
---
layout: false
class: normslide
.left-column[
  ## Wer hat uns verraten? METADATEN!
]
.right-column[
Daten über Daten:

 - Wer? Wo? Wie lange? mit wem? Wann? Wie oft? Wohin? (Worüber?)

 - u.U. interessanter als "Was?"

<img style="width:110%"  src="/assets/corres-6e8df25ae11f626a070cc46afba47ec3d653b1e2260c56fcfea955f698118ffc.png">
Momkai. Lizenz: Creative Commons BY-NC-SA 4.0
]
???
Notes for Presentation mode
---
layout: false
class: normslide
.left-column[
  ## Wer hat uns verraten? METADATEN!
]
.right-column[
------
* Bewegungsprofile
<img style="width:110%"  src="/assets/movement-c66d43cef2fb3a0ff0ae25e5f3aeb196ab7b757200ada7df5a5d5059ac37ac1f.gif">
Momkai. Lizenz: Creative Commons BY-NC-SA 4.0
]
???
Notes for Presentation mode
