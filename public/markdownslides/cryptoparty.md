name: inverse
layout: true
class: center, middle, inverse
---
# Cryptoparty @ Monis Rache 
---------
# Verschlüsselung und Überwachung

.footnote3[ [cryptoparty.coderat.cc](https://cryptoparty.coderat.cc/)]
---
layout: false
class: normslide
.left-column[
  ## Themen
]
.right-column[
-----

 - Was Metadaten über uns verraten

 - Threatmodelling / Bedrohungsszenarien 

 - Fragen und Themen sammeln


]
.footnote3[ [cryptoparty.coderat.cc](https://cryptoparty.coderat.cc/)]
???
Notes for Presentation mode

---
layout: false
class: normslide
.left-column[
  ## Wer hat uns verraten? METADATEN!
]
.right-column[
Daten über Daten:

 - Wer? Wo? Wie lange? mit wem? Wann? Wie oft? Wohin? (Worüber?)

 - u.U. interessanter als "Was?"

<img style="width:110%"  src="/assets/corres-6e8df25ae11f626a070cc46afba47ec3d653b1e2260c56fcfea955f698118ffc.png">
Momkai. Lizenz: Creative Commons BY-NC-SA 4.0
]
???
Notes for Presentation mode
---
layout: false
class: normslide
.left-column[
  ## Wer hat uns verraten? METADATEN!
]
.right-column[
------
* Bewegungsprofile
    
<img style="width:110%"  src="/assets/movement-c66d43cef2fb3a0ff0ae25e5f3aeb196ab7b757200ada7df5a5d5059ac37ac1f.gif">
Momkai. Lizenz: Creative Commons BY-NC-SA 4.0
]
???
Notes for Presentation mode
---
layout: false
class: normslide
.left-column[
  ## Threatmodelling?
]
.right-column[
--------------
W-Fragen der Bedrohungsscenarien:
 - Wer überwacht?
 
 Polizeibehörden / Geheimdienste / Cybercrime / Hackerkind von nebenan /  Werbenetzwerke

 - Was können die?

 Physischer Zugriff / Rechtliche Mittel (Zugriff auf Handynetz, Emailprovider) / Webtracking

 - Was wollen sie?
 
 Persönliche Daten / Geld / Social Graph / Usertracking


]
.footnote3[ [cryptoparty.coderat.cc](https://cryptoparty.coderat.cc/)]
???
Threatmodelling bedeutet sich zu überlegen, wer versucht dich zu überwachen. Das
führt direkt zu der Frage, über welche (technischen, personellen, finanziellen,
rechtlichen, etc.) Kapazitäten die Angreifer*in verfügt.
Das nächste Thema ist, was eigentlich das Ziel der Überwachung sein soll. Geht
es darum, bestimmte Daten von deinen Geräten zu sichern (z.B. belastende
Bilder)? Oder darum, den Kontakt zwischen dir und anderen Personen/Gruppen
nachzuweisen? Um den Diebstahl von Kontoinformationen? Oder darum, deine
Vorlieben herauszufinden (z.B. für targeted advertising).
---
layout: false
class: normslide
.left-column[
  ## Threatmodelling?
]
.right-column[
---------

W-Fragen des Threatmodelling:

 - Wie werden Aktionen gegen dich umgesetzt?
 
 Hacking / Geräte auslesen / Tracking beim Surfen

 - Was tun wir?
 
 Geräte verschlüsseln / sicher surfen / sichere Betriebssysteme / Ende-zu-Ende
    Verschlüsselung / Datensparsamkeit / Threatmodelling / vorsichtig sein / nicht paranoid werden / weiterlernen

]
.footnote3[ [cryptoparty.coderat.cc](https://cryptoparty.coderat.cc/)]
???
Aus diesen Fragen ergibt sich die Palette der Mittel, die die Angreifer*in
eventuell einsetzen könnte. Die Bandbreite ist dabei sehr groß: Hausdurchsung
mit Analyse von Festplatten, staatliches oder privates Hacking, Mitlesen von
Emails, Tracking des Surfverhaltens oder Überwachung des Anschlusses.
Aus diesen Überlegungen folgt der für uns praktisch relevante Punkt: Was tun?
---
layout: false
class: normslide
.left-column[
  ## Bedrohungsszenarien
  ###  Hausdurchsuchung
]
.right-column[
---------
Bei einer Aktion (z.B. Demo) wurdest du von der Polizei identifiziert und in
Zusammenhang mit verbotenen Handlungen gebracht.
Du und deine Gruppe habt Angst, demnächst Ziel einer Hausdurchsuchung zu werden.

- Kommen die Bullen an alle eure Daten, wenn sie eure Geräte mitnehmen?

- Wie sieht es aus, wenn die Festplatte verschlüsselt ist?

- Ist es wichtig, ob das Gerät ein- oder ausgeschaltet ist, wenn es mitgenommen wird?

- Solltet ihr eure Rechner ganz normal weiterbenutzen, wenn ihr sie wiederbekommt?

]
.footnote3[ [cryptoparty.coderat.cc](https://cryptoparty.coderat.cc/)]
???
Notes for Presentation mode
---
layout: false
class: normslide
.left-column[
  ## Bedrohungsszenarien
  ### Staatliche Überwachung
]
.right-column[
---------

Du bist bereits in den Fokus der Überwachung gerückt.
Die Polizei überwacht deine Emails bei deinem Emailprovider. 

- Können sie alle deine Mails einfach so lesen? 

- Gibt dir jemand Bescheid?

- Welche Informationen bekommen die Bullen noch, wenn du deine Mails verschlüsselst?

- Was sind eigentlich Metadaten?

]
.footnote3[ [cryptoparty.coderat.cc](https://cryptoparty.coderat.cc/)]
???
Notes for Presentation mode

---
layout: false
class: normslide
.left-column[
  ## Bedrohungsszenarien
  ### Staatliche Überwachung
]
.right-column[
---------

Die Polizei überwacht außerdem deinen Internetanschluss. 

- Sehen die jede Website, die du aufrufst?

- Welche Informationen fallen an, wenn die Verbindung zu Webseiten verschlüsselt
    stattfindet (Https/TLS)?

- Wie siehts mit Tor aus?

]
.footnote3[ [cryptoparty.coderat.cc](https://cryptoparty.coderat.cc/)]
???
Notes for Presentation mode
