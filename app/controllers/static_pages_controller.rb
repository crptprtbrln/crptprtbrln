class StaticPagesController < ApplicationController

  def show_landing_page
    render :landing_page
  end
  def show_video_page
    render :video_page
  end
end
