# set path to application
app_dir = File.expand_path("../..", __FILE__)
working_directory app_dir

rails_env = ENV['RAILS_ENV'] || 'production'

# Set unicorn options
worker_processes 2
preload_app true
timeout 30

# Set up socket location
#listen "/var/run/ratmap/unicorn.sock", :backlog => 64
listen(3000, backlog: 64) #if ENV['RAILS_ENV'] == 'development'

# Logging
stderr_path "#{app_dir}/log/unicorn.stderr.log"
stdout_path "#{app_dir}/log/unicorn.stdout.log"

# Set master PID location
pid "#{app_dir}/pids/unicorn.pid"
pid "/var/run/unicorn_cryptoparty.pid"
