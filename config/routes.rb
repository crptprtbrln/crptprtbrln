Rails.application.routes.draw do
	root 'static_pages#show_landing_page'
  get 'videos', to: 'static_pages#show_video_page'
  get 'slides', to: 'slide_show#show' 
end
